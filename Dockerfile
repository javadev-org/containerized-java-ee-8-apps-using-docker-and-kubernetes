#FROM maven:3.6.1-jdk-8-alpine as builder
FROM maven as builder

WORKDIR /app

COPY ./coffee-shop .
RUN mvn clean package

####################################

# FROM java:8-jdk-alpine
# FROM java:9-b149-jdk
FROM java
LABEL maintainer="marley.org"

# change version to current builds
ENV RELEASE 2019-03-15_2203
ENV VERSION 19.0.0.3-cl190320190315-1901

RUN wget https://public.dhe.ibm.com/ibmdl/export/pub/software/openliberty/runtime/nightly/${RELEASE}/openliberty-all-${VERSION}.zip \
        && unzip openliberty-all-${VERSION}.zip -d /opt \
        && rm openliberty-all-${VERSION}.zip

ENV DEPLOYMENT_DIR /opt/wlp/usr/servers/defaultServer/dropins/
ENV CONFIG_DIR /opt/wlp/usr/servers/defaultServer/

####################################

ENV JVM_ARGS="-Xmx512M"

COPY server.xml $CONFIG_DIR
COPY --from=builder /app/target/coffee-shop.war $DEPLOYMENT_DIR
# COPY postgresql-9.4-1201.jar /opt/wlp/lib/
RUN wget -O /opt/wlp/lib/postgresql-9.4-1201.jar \
        https://jdbc.postgresql.org/download/postgresql-9.4-1201.jdbc4.jar

CMD ["/opt/wlp/bin/server", "run", "defaultServer"]
